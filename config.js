var path = require('path');

module.exports = function(config) {
    let def = {
        entry: {},
        output: '',
        externals: {},
        watch: false,
        sourceMap: false,
        mode: 'production',
        plugins: []
    };
    return Object.assign(def, config);
}