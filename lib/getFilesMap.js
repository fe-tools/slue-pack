const path = require('path');
const xtend = require('xtend');
const slueModule = require('slue-module');
const chalk = require('chalk');
const utils = require('./utils');

let processArgvs = process.argv.slice(2);
let writeLog = processArgvs.indexOf('-l') != -1;
module.exports = function(config, modulesMap) {
    let filesMapGroupedByAppkey = {};
    let filePathList = [];
    let t1 = new Date().getTime();
    for (let key in config.entry) {
        let slueModConfig = xtend({
            filePath: path.resolve(process.cwd(), config.entry[key])
        }, config);

        let relayData = slueModule(slueModConfig);

        if (relayData) {
            filesMapGroupedByAppkey[key] = relayData.handledFile;
            filePathList = filePathList.concat(Object.keys(relayData.handledFile));
        }
    }

    if (config.output.commonPkgName) {
        filesMapGroupedByAppkey = getCommonPkg(filePathList, filesMapGroupedByAppkey, config.output.commonPkgName);
    }

    if (writeLog) {
        let t2 = new Date().getTime();
        console.log(chalk.blueBright(`********** module analysis total cost: ${(t2 - t1)/1000}s`));
    }

    return filesMapGroupedByAppkey;
}

function getCommonPkg(filePathList, filesMapGroupedByAppkey, commonPkgName) {
    let theCommonFileMap = {};
    theCommonFileMap = {};
    let modulesMap = {};
    filePathList.map(function(fp) {
        if (modulesMap[fp]) {
            theCommonFileMap[fp] = 1;
        } else {
            modulesMap[fp] = 1;
        }
    });

    for (let appkey in filesMapGroupedByAppkey) {
        let theFileMap = filesMapGroupedByAppkey[appkey];
        for (let fp in theFileMap) {
            if (theCommonFileMap[fp]) {
                theCommonFileMap[fp] = theFileMap[fp];
                //console.log(fp);
                delete theFileMap[fp];
            }
        }
    }

    if (filesMapGroupedByAppkey[commonPkgName]) {
        Object.assign(filesMapGroupedByAppkey[commonPkgName], theCommonFileMap)
    } else {
        filesMapGroupedByAppkey[commonPkgName] = theCommonFileMap;
    }

    return filesMapGroupedByAppkey;
}