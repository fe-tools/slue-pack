const fs = require('fs');
const os = require('os');
const path = require('path');
const md5 = require('md5');
const slueStream = require('slue-stream');
const sourceMap = require('source-map');
const utils = require('./utils');

function createConsumer(file) {
    return new Promise(function(resolve) {
        if (file.sourceMap) {
            sourceMap.SourceMapConsumer.with(file.sourceMap, null, function(consumer) {
                resolve({
                    contents: file.contents.toString(),
                    consumer: consumer
                });
            });
        } else {
            resolve({
                contents: file.contents.toString()
            });
        }
    });
}

module.exports = function(config = {}) {
    return slueStream.transformObj(function(file, env, cb) {
        let extname = path.extname(file.path);
        let moduleId = file.path;
        let snippet;
        if (extname !== '.css' && extname !== '.less' && extname !== '.sass') {
            moduleId = moduleId.replace(/\\/g, '/');
            let entryFileData = isEntryFile(file.path, config);
            if (entryFileData) {
                let publicPath = config.output.publicPath || '/';
                let jsPath = config.output.jsPath || '/';
                let _path = path.resolve(publicPath, './', jsPath, `${entryFileData.appKey}.js`);
                _path = _path.replace(/\w+:\\*/, '/').replace(/\\/g, '/');
                let contents = file.contents.toString();
                contents = `${contents}${os.EOL};module.exports.address='${_path}';${os.EOL}`;
                file.contents = Buffer.from(contents);
                moduleId = 'entry_file_module';
            }
            snippet = `'${moduleId}': function(__sluepack_require, exports, module) {${os.EOL}`;
        }

        createConsumer(file).then(function(res) {
            let _node = new sourceMap.SourceNode(null, null, null, null);
            if (res.contents && res.consumer) {
                _node = sourceMap.SourceNode.fromStringWithSourceMap(res.contents, res.consumer);
            } else if (res.contents) {
                _node.add(res.contents);
            }
            _node.prepend(snippet);
            _node.add(`\n}${os.EOL}`);

            let generator = _node.toStringWithSourceMap();

            let contents = new Buffer.from(_node.toString());
            fs.writeFileSync(`${utils.cacheDir}/tpls/${md5(file.path)}.tpl`, contents);

            let _map = generator.map.toString();
            let _mapPath = file.path.replace(/\\/g, '/');
            fs.writeFileSync(`${utils.cacheDir}/tpls/${md5(_mapPath)}.tpl.map`, _map);

            cb(null, file);
        });
    });
};

function isEntryFile(filePath, config) {
    let allEntryFiles = [];
    for (let appKey in config.entry) {
        if (typeof config.entry[appKey] == 'string') {
            if (filePath == path.resolve(process.cwd(), config.entry[appKey])) {
                return {
                    appKey
                };
            }
        }
        if (Array.isArray(config.entry[appKey])) {
            let allEntryFiles = config.entry[appKey].map(function(filePath) {
                return path.resolve(process.cwd(), filePath);
            });
            if (allEntryFiles.includes(filePath)) {
                return {
                    appKey
                };
            }
        }
    }
}