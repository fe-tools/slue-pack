const fs = require('fs');
const os = require('os');
const path = require('path');
const md5 = require('md5');
const sourceMap = require('source-map');
const utils = require('./utils');

let regular = /(\s*)'((\w:)?\/([\w\-]+\/)+[\w\-]+\.[\w]+)':\s*function\(__sluepack_require/;
module.exports = function(src) {
    let _sourceContent = fs.readFileSync(src, 'utf8');
    let sourceContent = _sourceContent.split(/\r?\n/);
    let _sourcemap = new sourceMap.SourceMapGenerator({
        file: src
    });
    sourceContent.forEach(function(code, line) {
        let matchRes = code.match(regular);
        if (matchRes) {
            let soureMapName = `${md5(matchRes[2])}.tpl.map`;
            let soureMapSrc = path.resolve(utils.tplCacheDir, `./${soureMapName}`);
            if (fs.existsSync(soureMapSrc)) {
                let sourceMapContent = fs.readFileSync(soureMapSrc, 'utf8');
                let whatever = sourceMap.SourceMapConsumer.with(sourceMapContent, null, function(consumer) {
                    let source;
                    consumer.eachMapping(function(mapping) {
                        if (mapping.generatedLine &&
                            mapping.generatedColumn &&
                            mapping.originalLine &&
                            mapping.originalColumn
                        ) {
                            _sourcemap.addMapping({
                                generated: {
                                    line: mapping.generatedLine + line,
                                    column: mapping.generatedColumn + matchRes[1].length
                                },
                                source: mapping.source,
                                original: {
                                    line: mapping.originalLine,
                                    column: mapping.originalColumn
                                }
                            });
                            source = mapping.source;
                        }
                    });
                    _sourcemap.setSourceContent(source, consumer.sourceContentFor(source));
                    fs.writeFileSync(`${src}.map`, _sourcemap.toString());
                });
            }
        }
    });

    let basename = path.basename(src);
    _sourceContent = `${_sourceContent}${os.EOL}//# sourceMappingURL=${basename}.map`;
    fs.writeFileSync(src, _sourceContent);
}