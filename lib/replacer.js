const path = require('path');
const slueStream = require('slue-stream');

module.exports = function(config) {
    return slueStream.transformObj(function(file, env, cb) {
        let moduleRegular = /([^\w])(?:require\(('|")([@\w./-]+)\2\)|(?:import|export)\s*(?:[\w\s,\-*{}]+)?from\s*('|")([@\w./-]+)\4)/g;
        let itemRegular = /([^\w])(?:require\(('|")([@\w./-]+)\2\)|(?:import|export)\s*(?:[\w\s,\-*{}]+)?from\s*('|")([@\w./-]+)\4)/;

        let contents = file.contents.toString();
        contents = ` ${contents}`;
        contents = contents.replace(moduleRegular, function(item) {
            let itemMatchRes = item.match(itemRegular);
            if (itemMatchRes) {
                let moduleId = itemMatchRes[3] || itemMatchRes[5];
                if (config.externals && config.externals[moduleId]) {
                    item = `${itemMatchRes[1]}__sluepack_require('${moduleId}')`;
                } else {
                    let fileMap = file.filesMap[file.path];
                    let filePath = fileMap[moduleId];
                    if (filePath) {
                        let extname = path.extname(filePath);
                        if (extname === '.css' || extname === '.less' || extname === '.sass') {
                            item = itemMatchRes[1];
                        } else {
                            let _moduleId = filePath.replace(/\\/g, '/');
                            if (_moduleId) {
                                item = `${itemMatchRes[1]}__sluepack_require('${_moduleId}')`;
                            } else {
                                item = item.replace(new RegExp(moduleId), _moduleId);
                            }

                        }
                    }
                }
            }
            return item;
        });

        file.contents = new Buffer.from(contents.replace(/^\s/, ''));

        cb(null, file);
    });
}