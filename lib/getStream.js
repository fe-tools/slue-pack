const path = require('path');
const Bluebird = require('bluebird');
const slueFS = require('slue-fs');
const slueStream = require('slue-stream');
const vueCompiler = require('slue-vue-compiler');
const lodash = require('lodash');
const prevHandle = require('./prevHandle');
const cssHandler = require('./csshandler');

module.exports = function(filesMapGroupedByAppkey, config) {
    let promise = new Bluebird(function(resolve, reject) {
        let filesNumber = 0;
        let streams = [];
        for (let appKey in filesMapGroupedByAppkey) {
            let filesMap = filesMapGroupedByAppkey[appKey];
            let filesMapGroupedByExtName = groupByExtName(filesMap);

            for (let extname in filesMapGroupedByExtName) {
                let files = Object.keys(filesMapGroupedByExtName[extname]);
                filesNumber += files.length;
                let stream = slueFS.read(files, {
                        filesMap: filesMap,
                        entryName: appKey
                    }).pipe(prevHandle({
                        envs: {
                            NODE_ENV: config.mode
                        }
                    }))
                    .pipe(vueCompiler({
                        sourceMap: config.sourceMap
                    }))
                    .pipe(cssHandler());

                // handle config.plugins
                if (config.plugins) {
                    config.plugins.forEach(function(item) {
                        if (item.exts.includes(extname)) {
                            item.use.forEach(function(plugin) {
                                stream = stream.pipe(plugin());
                            });
                        }
                    });
                }

                streams.push(stream);
            }
        }

        if (streams.length) {
            resolve({
                stream: slueStream.combine(streams),
                filesNumber: filesNumber
            });
        } else {
            reject();
        }
    });
    return promise;
};

function groupByExtName(filesMap) {
    let data = {};
    for (let filePath in filesMap) {
        let extname = path.extname(filePath);
        data[extname] = data[extname] || {};
        data[extname][filePath] = filesMap[filePath];
    }
    return data;
};