const path = require('path');
const slueStream = require('slue-stream');
const babel = require('babel-core');
const UglifyJS = require('uglify-es');

module.exports = function(opts = {}) {
    if (typeof opts.useBabel !== 'boolean') {
        opts.useBabel = true;
    }
    return slueStream.transformObj(function(file, env, cb) {
        let extname = path.extname(file.path);
        if (extname === '.js') {
            let hasVariables = false;
            let contents = file.contents.toString();
            for (let env in opts.envs) {
                let regular = new RegExp(`process\\\.env\\\.${env}[^z-zA-Z_0-9]`, 'g');
                contents = contents.replace(regular, function(item) {
                    hasVariables = true;
                    let _regular = new RegExp(`process\\\.env\\\.${env}`, 'g');
                    item = item.replace(_regular, `\'${opts.envs[env]}\'`);
                    return item;
                });
            }

            if (hasVariables) {
                try {
                    let res = UglifyJS.minify(contents, {
                        output: {
                            beautify: true
                        }
                    });
                    if (res.error) {
                        res.error.filePath = file.path;
                        console.log(res.error);
                    } else {
                        contents = res.code;
                    }
                } catch (e) {
                    console.log(e);
                }
            }

            try {
                if (file.path.indexOf('node_modules') == -1) {
                    let result = babel.transform(contents, {
                        compact: false,
                        sourceMaps: true,
                        presets: [
                            require('babel-preset-env'),
                            require('babel-preset-react'),
                            require('babel-preset-stage-3')
                        ],
                        plugins: [
                            require('babel-plugin-syntax-dynamic-import')
                        ]
                    });

                    contents = result.code;

                    if (result.map) {
                        let pcwd = path.resolve(process.cwd(), '../');
                        let relativePath = path.relative(pcwd, file.path);
                        let moduleId = relativePath.replace(/\\/g, '/');
                        result.map.sources = [`sluepack:///${moduleId}`];
                        file.sourceMap = result.map;
                        file.sourceMap.file = file.path;
                    }
                }
                
            } catch (e) {
                e.filePath = file.path;
                console.log(e);
            }
            file.contents = Buffer.from(contents);

            cb(null, file);
        } else {
            cb(null, file);
        }
    });
}