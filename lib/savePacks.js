const fs = require('fs');
const os = require('os');
const fse = require('fs-extra');
const path = require('path');
const md5 = require('md5');
const beautify = require('js-beautify');
const lodash = require('lodash');
const utils = require('./utils');
const sourceMap = require('source-map');

process.on('unhandledRejection', function() {});

function deepClone(obj) {
    return JSON.parse(JSON.stringify(obj));
}

function createConsumer(opts) {
    return new Promise(function(resolve) {
        sourceMap.SourceMapConsumer.with(opts.map, null, function(consumer) {
            opts.consumer = consumer;
            resolve(opts);
        });
    });
}

function sequence(list, appKey, config) {
    let result = {
        packJsSrcNodes: [],
        packCssSrc: [],
        appKey: appKey
    };
    let promise = new Promise(function(resolve) {
        resolve();
    });
    list.forEach(function(item) {
        promise = promise.then(function() {
            return concatSnippet(item, result, config);
        });
    });
    return promise;
}

function concatSnippet(filePath, result, config) {
    return new Promise(function(resolve) {
        let extname = path.extname(filePath);

        let contents;
        if (config.externals && config.externals[filePath]) {
            contents = `'${filePath}': function(__sluepack_require, exports, module) {module.exports = ${config.externals[filePath]}}`
        } else {
            contents = fs.readFileSync(`${utils.tplCacheDir}/${md5(filePath)}.tpl`, 'utf8');
        }

        if (utils.isCssFile(extname)) {
            let contents = fs.readFileSync(`${utils.tplCacheDir}/${md5(filePath)}.tpl`, 'utf8');
            result.packCssSrc.push(contents);
            resolve(result);
        } else {
            let moduleId = filePath.replace(/\\/g, '/');
            let fileMapPath = `${utils.tplCacheDir}/${md5(moduleId)}.tpl.map`;
            if (fs.existsSync(fileMapPath)) {
                createConsumer({
                    code: contents,
                    map: fs.readFileSync(fileMapPath, 'utf8')
                }).then(function(res) {
                    let node = sourceMap.SourceNode.fromStringWithSourceMap(res.code, res.consumer);
                    node.add(`,${os.EOL}`);
                    result.packJsSrcNodes.push(node);
                    resolve(result);
                });
            } else {
                let node = new sourceMap.SourceNode(null, null, null, `${contents},${os.EOL}`);
                result.packJsSrcNodes.push(node);
                resolve(result);
            }
        }
    });
}

module.exports = function(config, filesMapGroupedByAppkey) {
    let me = this;
    if (config.injectTo) {
        let htmlPath = path.resolve(process.cwd(), config.injectTo);
        var fileHtml = fs.readFileSync(htmlPath, 'utf8');
    }
    config.output.jsPath = config.output.jsPath || './';
    config.output.staticPath = config.output.staticPath || './';
    let jsPath = path.resolve(config.output.root, config.output.jsPath);
    let staticPath = path.resolve(config.output.root, config.output.staticPath);
    fse.mkdirsSync(jsPath);

    let packTpl = fs.readFileSync(`${utils.libDir}/template.tpl`, 'utf8');
    for (let appKey in config.entry) {
        let filesMap = filesMapGroupedByAppkey[appKey];
        let appRelayFilesPath = Object.keys(filesMap);
        for (let fp in filesMap) {
            if (filesMap[fp].externals) {
                appRelayFilesPath = appRelayFilesPath.concat(Object.keys(filesMap[fp].externals))
            }
        }
        sequence(deepClone(appRelayFilesPath), appKey, config).then(function(result) {
            let appKey = result.appKey;
            let packJsSrcNodes = result.packJsSrcNodes;
            let packCssSrc = result.packCssSrc;
            let packPath = path.resolve(jsPath, './', `${appKey}.js`);

            let node = new sourceMap.SourceNode(null, null, null, packJsSrcNodes);
            node.prepend(packTpl);
            node.add(`});${os.EOL}`);
            if (config.module == 'cmd') {
                node.prepend(`define(function(require, exports, module) {${os.EOL}`)
                node.add('});');
            }

            if (config.sourceMap === true) {
                node.add(`${os.EOL}//# sourceMappingURL=${appKey}.js.map`);
            }

            let jsPkgContent = node.toString();
            if (config.sourceMap === false) {
                jsPkgContent = beautify(jsPkgContent, {
                    indent_size: 4
                });
            }

            fs.writeFileSync(packPath, jsPkgContent);
            let address = path.resolve('/', config.output.jsPath || '', `${appKey}.js`);
            address = address.replace(/\w+:\\*/, '/').replace(/\\/g, '/')
            me.assets = me.assets || {};
            me.assets[appKey] = {
                path: packPath,
                address: `${config.output.publicPath || ''}${address}`
            };
            me.vars = me.vars || {};
            me.vars[appKey] = me.assets[appKey].address;

            if (fileHtml) {
                let injectPath = path.relative(config.output.root, packPath);
                fileHtml = fileHtml.replace(/<%\s*sluepack\.assets\.([\w\-]+)\s*%>/g, function(res, name) {
                    if (me.assets[name]) {
                        return me.assets[name].address;
                    }
                    return res;
                })
            }

            if (config.sourceMap === true) {
                let generator = node.toStringWithSourceMap();
                fs.writeFileSync(`${packPath}.map`, generator.map.toString());
            }

            if (packCssSrc.length) {
                fse.mkdirsSync(`${staticPath}`);
                let packPath = `${staticPath}/${appKey}.css`;
                fs.writeFileSync(packPath, packCssSrc.join(os.EOL));
                if (fileHtml) {
                    let injectPath = path.relative(config.output.root, packPath);
                    let htmlFragment = `<style src="${injectPath}"></style></head>`;
                    fileHtml = fileHtml.replace(/<\/head>/, htmlFragment);
                }
            }

            if (fse.emptyDirSync(utils.imgCacheDir)) {
                fse.copySync(utils.imgCacheDir, `${staticPath}/images`);
            }

            if (fileHtml) {
                let htmlFileName = path.basename(config.injectTo);
                fs.writeFileSync(path.resolve(config.output.root, './', htmlFileName), fileHtml);
            }
        });
    }
}