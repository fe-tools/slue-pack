# slue-pack

A web module bounder.

## API
```javascript
const sluePack = require('slue-pack');
sluePack(options);
```

## options:
```javascript
var path = require('path');
module.exports = {
    // the map of package name and it's entry file
    entry: {
        app: './test/app.js'
    },

    // output of slue
    output: {
        // the root path of your output files 
        root: path.resolve(__dirname, './dev'),
        // path of js files(relative to root)
        jsPath: './',
        // other files path(relative to root)
        staticPath: './dev/static/',
        // if entry has many files, the common module pkg name, can be one of the entry
        commonPkgName: 'app'
    },

    // external global variables
    externals: {
        'react': 'React'
    },

    // when exclude return true, the required module will be ignore
    exclude: function(opts) {
        return opts.moduleId == 'xxx';
    },

    watch: true,

    // create sourceMap when sourceMap is true, default false
    sourceMap: true,

    // compile environment，development or production
    mode: 'production',

    // the type of output files.
    module: 'cmd',

    // alias make module require shorter 
    alias: {
        components: path.join(__dirname, './components')
    },

    // extension, used to compile the type of file which  
    plugins: [{
        exts: ['.js'], // files ext names
        use: [getOnePlugin, getOnePlugin] // plugins list
    }]
};
```