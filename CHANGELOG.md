# slue-vue-compiler changelog

## 1.4.3

### update slue-vue-compiler

## 1.4.4

### update slue-envify

## 1.4.5

### update slue-envify
### update slue-babel
### set useBabel param false to slue-envify

## 1.4.6

### filte the slue-module result if the moduleId has already in modulesMap.

## 1.4.8

### fix the bug when edit the entry file.

## 1.5.0

### support sourcemap.

## 1.5.1

### vue support sourcemap.

## 1.5.2

### bugfix.

## 1.5.3

### vue sourcemap bugfix.

## 1.5.4

### vue sourcemap bugfix.

## 1.5.5

### vue sourcemap.

## 1.5.6

### sourcemap bugfix.

## 1.5.8

### babel and node variables.

## 1.5.9

### getStream bugfix.

## 1.6.0

### the entry file's sourceMap.

## 1.6.1

### umd.

## 1.6.2

### the template of the end package.

## 1.6.3

### entryFiles bugfix when more than one pack.

## 1.6.4

### sourcemap path add project folder.

## 1.6.5

### add watcherchange event when the sluepack's wathcer change.

## 1.6.6

### output variables.

## 1.6.7

### handle require.async.

## 1.6.8

### handle require.async.

## 1.6.9

### injectTo.