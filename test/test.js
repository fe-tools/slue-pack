const fs = require('fs');
const path = require('path')
const sluePack = require('../index');
const slueStream = require('slue-stream');

const getOnePlugin = function() {
    return slueStream.transformObj(function(file, env, cb) {
        cb(null, file);
    });
}

sluePack({
    entry: {
        app: './test/app1.js'
    },
    externals: {
        'react': 'React'
    },
    plugins: [{
        exts: ['.js'],
        use: [getOnePlugin, getOnePlugin]
    }],
    output: {
        // 文件输出的根目录
        root: path.resolve(__dirname, './build'),
        // js文件输出目录(相对root目录)
        jsPath: './',
        // 除js文件外，其他静态文件输出目录，包括的包出的css、图片等文件、字体文件等(相对root目录)
        staticPath: './static/'
    }
});