/**
 * file: moudles.js
 * ver: 1.0.0
 * auth: 471665225@qq.com
 * update: 21:12 2016/8/2
 */

(function(_modules) {
    var head = document.getElementsByTagName('head')[0];

    var __web__bound = window.__web__bound = window.__web__bound || {
        modules: {},
        // 模块缓存
        modulesCache: {},
        // 用来处理__sluepack_require.async的回调
        loadingMap: {}
    };

    var __modulesCache = __web__bound.modulesCache;
    var __modules = __web__bound.modules;
    var __loadingMap = __web__bound.loadingMap;

    var __sluepack_require = function(id) {
        id = __sluepack_require.alias(id);
        var mod = __modulesCache[id];
        if (mod) {
            return mod.exports;
        }
        mod = __modulesCache[id] = {
            'exports': {}
        };

        var factory = __modules[id];
        if (id == 'entry_file_module') {
            factory = entryFileFactory;
        }
        if (typeof factory === 'undefined') {
            throw Error('Cannot find module `' + id + '`');
        }
        var ret = factory.apply(mod, [__sluepack_require, mod.exports, mod]);
        if (ret) {
            mod.exports = ret;
        }
        if (id == 'entry_file_module' && mod.exports.address) {
            __modulesCache[mod.exports.address] = mod;
        }
        delete __modulesCache.entry_file_module;
        return mod.exports;
    };

    __sluepack_require.async = function(ids, callback) {
        if (typeof ids === 'string') {
            ids = [ids];
        }

        var loadingNum = 0;

        for (var i = ids.length - 1; i >= 0; --i) {
            var id = __sluepack_require.alias(ids[i]);
            __loadingMap[id] = function() {
                loadingNum--;
                if (0 == loadingNum) {
                    var i, n, args = [];
                    for (i = 0, n = ids.length; i < n; ++i) {
                        args[i] = __sluepack_require(ids[i]);
                    }
                    callback && callback.apply(self, args);
                }
            };
            loadingNum++;
            loadScript(id);
        }
    };

    __sluepack_require.import = function(ids) {
        return new Promise(function(resolve) {
            __sluepack_require.async(ids, function(args) {
                resolve(args);
            });
        });
    };

    __sluepack_require.alias = function(id) {
        return id
    };

    var onload = function(id, script) {
        __loadingMap[id] && __loadingMap[id]();
        script.parentNode.removeChild(script);
    };
    var loadScript = function(id) {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = id;
        script.onload = onload.bind(null, id, script);
        head.appendChild(script);
    }

    var entryFileFactory = _modules.entry_file_module;
    delete _modules.entry_file_module;
    for (var m in _modules) {
        __modules[m] = _modules[m];
    }

    var absoluteuri = 'entry_file_module';
    if (__web__bound.loadingMap[absoluteuri]) {
        __web__bound.loadingMap[absoluteuri]();
    }

    if (typeof exports === 'object') {
        module.exports = __sluepack_require(absoluteuri);
        if (module.exports.__esModule && module.exports.default) {
            module.exports = module.exports.default;
        }
    } else {
        return __sluepack_require(absoluteuri);
    }
})({
    'entry_file_module': function(__sluepack_require, exports, module) {
        'use strict';

        var _pagea = __sluepack_require('/Users/songgl/workspace/selfworks/slue-pack/test/pagea/index.js');

        var _pagea2 = _interopRequireDefault(_pagea);

        var _pageb = __sluepack_require('/Users/songgl/workspace/selfworks/slue-pack/test/pageb/index.js');

        var _pageb2 = _interopRequireDefault(_pageb);

        var _react = __sluepack_require('react');

        var _react2 = _interopRequireDefault(_react);

        function _interopRequireDefault(obj) {
            return obj && obj.__esModule ? obj : {
                default: obj
            };
        };
        module.exports.address = '/app.js';

    },
    '/Users/songgl/workspace/selfworks/slue-pack/test/pagea/index.js': function(__sluepack_require, exports, module) {
        'use strict';

        var pagea = __sluepack_require('/Users/songgl/workspace/selfworks/slue-pack/test/pagea/config.js');

        module.exports = pagea;
    },
    '/Users/songgl/workspace/selfworks/slue-pack/test/pagea/config.js': function(__sluepack_require, exports, module) {
        'use strict';

        module.exports = function() {
            console.log('page a');
        };
    },
    '/Users/songgl/workspace/selfworks/slue-pack/test/pageb/index.js': function(__sluepack_require, exports, module) {
        'use strict';

        var pageb = __sluepack_require('/Users/songgl/workspace/selfworks/slue-pack/test/pageb/config.js');

        module.exports = pageb;
    },
    '/Users/songgl/workspace/selfworks/slue-pack/test/pageb/config.js': function(__sluepack_require, exports, module) {
        'use strict';

        module.exports = function() {
            console.log('page b');
        };
    },
    'react': function(__sluepack_require, exports, module) {
        module.exports = React
    },
});