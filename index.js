const fs = require('fs');
const path = require('path');
const fse = require('fs-extra');
const xtend = require('xtend');
const chalk = require('chalk');
const lodash = require('lodash');
const Bluebird = require('bluebird');
const slueFs = require('slue-fs');
const getConfig = require('./config');
const utils = require('./lib/utils');
const getFilesMap = require('./lib/getFilesMap');
const getStream = require('./lib/getStream');
const savePacks = require('./lib/savePacks');
const Main = require('./lib/main');

let processArgvs = process.argv.slice(2);
let writeLog = processArgvs.indexOf('-l') != -1;

module.exports = function(config) {
    let me = this;
    config = getConfig(config);
    if (config.mode === 'production' || config.mode === 'development') {
        process.env.NODE_ENV = config.mode;
    }

    utils.clearCache();

    let filesMapGroupedByAppkey = getFilesMap(config);

    let compiler;
    let watcher;
    let promise = new Bluebird(function(resolve) {
        let t1 = new Date().getTime();
        getStream(filesMapGroupedByAppkey, config).then(function(data) {
            data.config = config;
            compiler = new Main(data);
            if (watcher) {
                compiler.watcher = watcher;
                watcher.on('watcherchange', function() {
                    compiler.emit('watcherchange');
                });
            }
            compiler.on('ok', function(data) {
                savePacks.bind(me)(config, filesMapGroupedByAppkey);
                resolve(compiler);
                if (writeLog) {
                    let t2 = new Date().getTime();
                    console.log(chalk.blueBright(`********** compile total cost ${(t2 - t1)/1000}s`));
                }
            });
        })
    });

    if (config.watch === true) {
        let allFile = [];
        for (let appKey in filesMapGroupedByAppkey) {
            let filesMap = filesMapGroupedByAppkey[appKey];
            allFile = allFile.concat(Object.keys(filesMap));
        }

        watcher = slueFs.watch(allFile);
        watcher.on('change', function(filePath, stat) {
            if (!utils.isCssFile(path.extname(filePath))) {
                let relativePath = path.relative(process.cwd(), filePath);
                console.log(chalk.blueBright(`compile Starting: ${relativePath}`))
                let startTime = new Date().getTime();

                let theChangedApp = findTheChangedApp(filePath, filesMapGroupedByAppkey, config);

                let _config = xtend({}, config);
                _config.entry = {
                    __watcherFile: filePath
                };

                let _filesMapGroupedByAppkey = getFilesMap(_config);

                // merge filesMap
                for (let fp in _filesMapGroupedByAppkey.__watcherFile) {
                    filesMapGroupedByAppkey[theChangedApp.appKey][fp] = _filesMapGroupedByAppkey.__watcherFile[fp]
                }

                getStream(_filesMapGroupedByAppkey, config).then(function(data) {
                    data.config = config;
                    let mainInstance = new Main(data);
                    mainInstance.on('ok', function(data) {
                        savePacks(config, filesMapGroupedByAppkey);
                        console.log(chalk.blueBright(`after ${new Date().getTime() - startTime}ms`));
                        console.log(chalk.blueBright(`compile Finished: ${relativePath}`))
                    });
                });
            }
            watcher.emit('watcherchange');
        });
    }

    return promise;
};

function findTheChangedApp(filePath, filesMapGroupedByAppkey, config) {
    for (let appKey in filesMapGroupedByAppkey) {
        let filesMap = filesMapGroupedByAppkey[appKey];
        if (filesMap[filePath]) {
            let entryFile = path.resolve(process.cwd(), config.entry[appKey]);
            return {
                entryFile,
                appKey
            };
        }
    }
}